---
layout: post
title:  Nouveaux tarifs
date:   2024-12-29
tags:   [Shiatsu]
---

## pour que le shiatsu et le do-in soient accessibles à tous

Nouveaux tarifs / troc / échange

### Une séance de Shiatsu (1h/1h15)

- Tarif solidaire : 1500F à 3000F ou troc 
- Tarif standard : 5000F à 7000F 
- Tarif de soutien : 8000F à 10000F  
- En points JEU (Jardin d’Echange Universel) 250∞

La Mutuelle des Fonctionnaires rembourse 2 séances de shiatsu par an à hauteur de 2500F par séance.


### une séance de do-in (1 heure)

- Tarif solidaire : 500F ou troc   
- Tarif standard : 1000F à 1500F
- Tarif de soutien : 2000F    
- En points JEU (Jardin d’Echange Universel) 100∞ 

**_Pour que le shiatsu et le do-in vous permettent de prendre en main votre santé il est préférable de s’engager sur plusieurs séances._**
