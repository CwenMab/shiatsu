---
layout: post
title:  Faciliter et accompagner l’activité sportive
date:   2021-09-25
tags:   [Shiatsu]
---

**L’activité physique régulière participe au même titre que l’alimentation et la respiration à notre hygiène de vie ; elle renforce notre santé.**

Vous souhaitez reprendre une activité physique, les automassages et les étirements du do-in seront un précieux sésame :

- vos muscles et vos articulations vont être mobilisés et échauffés tout en douceur.
- votre respiration sera facilitée et votre tonicité renforcée

**Le shiatsu viendra en complément soutenir votre motivation.**

Vous faites régulièrement du sport et vous avez besoin de vous préparer aux exigences d’une compétition, le shiatsu et le do-in peuvent faciliter votre entrainement et votre récupération :

- relâchement musculaire
- souplesse articulaire
- oxygénation des muscles et des tendons par une activation de la 
- circulation sanguine et de la respiration
- préparation mentale  
- élimination des toxines et diminution des courbatures
- meilleure récupération

Pour cela, je propose un programme personnalisé incluant 4 séances d’1heure.
- 2 shiatsu et 2 do-in pour un coût de 15 000F
- A organiser sur une période de 6 à 8 semaines.

