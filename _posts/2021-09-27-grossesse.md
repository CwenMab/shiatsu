---
layout: post
title:  Shiatsu et Maternite
date:   2021-09-27
tags:   [Shiatsu]
---

Toutes les étapes de la maternité, désir d’enfant, grossesse, accouchement et post partum peuvent être accompagnées par le shiatsu. Le shiatsu vient en complément des suivis médicaux habituels dont les prescriptions et recommandations doivent être strictement respectées. 
Je suis psychomotricienne et spécialiste en shiatsu*. Mon expérience dans le domaine de la maternité me permet de vous proposer un accompagnement personnalisé tout au long de votre grossesse et après l’accouchement.   

* Vous avez un désir d’enfant et il est important de vous préparer sur le plan physique, mental et émotionnel à cette nouvelle étape de votre vie.
* Vous souhaitez profiter de votre grossesse pour développer une connexion étroite avec votre corps et votre bébé. 
* Vous ressentez le besoin de vous ressourcer et de vous relaxer. 
* Vous recherchez un soulagement des troubles courants de la grossesse : maux de dos, nausées, crampes, œdème, brûlure d’estomac, constipation, essoufflement…
* Vous voulez vous préparer à l’accouchement : faciliter le travail et limiter la douleur…
* Après l’accouchement, vous souhaitez améliorer les processus de récupération sur le plan physique et psychique. 
* Vous désirez apaiser votre bébé et continuer à développer avec lui la communication par le toucher. 

Nous pouvons définir ensemble un programme de plusieurs séances qui réponde au mieux à vos attentes. 
Les séances de Shiatsu peuvent s’adresser à la mère seule, aux couples ou aux bébés accompagnés de l’un des parents. Elles seront combinées avec des exercices physiques et respiratoires et de l’auto shiatsu qui vous permettront de vous approprier de façon autonome les bienfaits de cette pratique. 

Je vous reçois à mon cabinet, 386 Impasse des Inséparables à Mont-Dore Sud, le jeudi et le vendredi de 10h à 18h. Je peux également me déplacer à votre domicile le mardi après-midi.

*Le titre de Spécialiste en Shiatsu est la certification d’Etat, délivrée par le SPS (Syndicat des Professionnels du Shiatsu), et aujourd’hui seul titre reconnu en France par les institutions. Il est enregistré au RNCP (Registre National de la Certification Professionnelle). Remboursé partiellement par la Mutuelle des Fonctionnaire en Nouvelle-Calédonie 
